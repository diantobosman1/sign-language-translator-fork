import React from 'react';
import Translator from '../components/Translator/Translator';

//-- This is used to depict the translator screen
const TranslatorView = () => {
  return (
    <div>
      <Translator />
    </div>
  )
}

export default TranslatorView;