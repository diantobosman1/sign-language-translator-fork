import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route, Router } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import StartView from './views/StartView';
import TranslatorView from './views/TranslatorView';
import ProfileView from './views/ProfileView';
import NotFound from './components/NotFound/NotFound';
import AppContainer from './hoc/AppContainer';
import { UserContext } from './Contexts/UserContext';
import { useState } from 'react';
import { TranslationContext } from './Contexts/TranslationContext';
import { LoginStateContext } from './Contexts/LoginStateContext';

function App() {

  //-- Set the ContextApi constants
  const [credentials, setCredentials] = useState({
    id: '',
    username: '',
    translations: []
  })

  const [ translation, setTranslation ] = useState([]);
  const [ loginState, setLoginState ] = useState(false)

  return (
    <div className="App">
      <LoginStateContext.Provider value={{loginState, setLoginState}}>
        <TranslationContext.Provider value={{ translation, setTranslation }}>
          <UserContext.Provider value={{ credentials, setCredentials }}>
            <Navbar />
              <AppContainer>
                <Routes>
                  <Route path="/" element={<StartView />} />
                  <Route path="/translator" element={<TranslatorView />} />
                  <Route path="/profile" element={<ProfileView />} />
                  <Route path="*" element={<NotFound />} />
                </Routes>
              </AppContainer>
          </UserContext.Provider>
        </TranslationContext.Provider>
      </LoginStateContext.Provider>
    </div>
  );
}

export default App;
