import { apiURL } from "../../api/index"
import { apiKey } from "../../api/index"

export async function updateTranslation(userId, newTranslation) {
    fetch(`${apiURL}/translations/${userId}`, {
        method: 'PATCH', // NB: Set method to PATCH
        headers: {
            'X-API-Key': apiKey,
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            //-- Set the translations to the new ones
            translations: newTranslation
        })
    })
    .then(response => {
    if (!response.ok) {
        throw new Error('Could not update translations history')
    }
    return response.json()
    })
    .then(updatedUser => {
    // updatedUser is the user with the Patched data
    })
    .catch(error => {
    })
}