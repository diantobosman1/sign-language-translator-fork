import React from 'react';
import { NavLink } from 'react-router-dom';

//--- This is used for when the user types an incorrect url
const NotFound = () => {
  return (
    <main>
      <h3>Page not found</h3>
      <NavLink to="/">Take me back to Login page</NavLink>
    </main>
  )
}

export default NotFound;