import React from 'react';
import { useEffect} from 'react'
import { loginUser } from './LoginApi';
import { useNavigate } from 'react-router-dom'
import { useContext } from 'react';
import { UserContext } from '../../Contexts/UserContext';
import { LoginStateContext } from '../../Contexts/LoginStateContext';
import { TranslationContext } from '../../Contexts/TranslationContext'

const Login = () => {
    //-- ContextApi imports
    const { credentials, setCredentials } = useContext(UserContext)
    const { loginState, setLoginState } = useContext(LoginStateContext)
    const { translation, setTranslation } = useContext(TranslationContext)

    //-- Router
    const navigate = useNavigate()

    //-- Side Effects
    useEffect(() => {
      if (loginState === true) {
          navigate('/translator')
      }
        
    }, [loginState])

    //-- Event handlers
    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })
    }

    const onFormSubmit = event => {
        //-- Stop the page reload
        event.preventDefault();
    
        //-- Login and set the state accordingly
        setLoginState(false);
        loginUser(credentials, setCredentials);
        setLoginState(true);

    }

    
    return (
      <>
        <div className="pt-4 pb-4">
          <h1>Sign Language Translator</h1>
        </div>

        <form className="mt-3" onSubmit= { onFormSubmit }>
          
          <div className="mb-3">
              <label htmlFor="username" className="form-label">Username</label>
              <input 
              id="username" 
              type="text" 
              placeholder="Enter your username" 
              className="form-control"
              onChange = {onInputChange}
              />
          </div>

          <button type="submit" className="btn btn-primary btn-lg">Login</button>
        </form>
      </>     
    )
}

export default Login;
